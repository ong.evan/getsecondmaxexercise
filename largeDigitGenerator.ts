function largeDigitGenerator(digits: number) : string {
    let digit = 1;
    let output = "";
    for (let i = 0; i < digits; i++) {
        output += digit;
        digit = digit === 9 ? 0 : digit + 1;
    }
    return output;
}

export default largeDigitGenerator;