import { compareIntegerString } from "./getSecondMax";

test('compareIntegerString - left isNaN', () => {
    const left = `1nvalid`;
    const right = `1000000`;
    expect(compareIntegerString(left, right)).toBe(1)
})

test('compareIntegerString - right isNaN', () => {
    const left = `1000000`;
    const right = `1nvalid`;
    expect(compareIntegerString(left, right)).toBe(-1)
})

test('compareIntegerString - left and right isNaN', () => {
    const left = `one thousand`;
    const right = `1nvalid`;
    expect(compareIntegerString(left, right)).toBeNaN();
})

test('compareIntegerString - left is +, right is -', () => {
    const left = `999999999999999999999999999999999999999999`;
    const right = `-9999999999999999999999999999999999999999`;
    expect(compareIntegerString(left, right)).toBe(1);
})

test('compareIntegerString - left is -, right is +', () => {
    const left  = `-9999999999999999999999999999999999999999`;
    const right = `99999999999999999999999999999999999999999`;
    expect(compareIntegerString(left, right)).toBe(-1);
})

test ('compareIntegerString - left is less than right (both positive)', () => {
    const left  =  `99999999999999999999999999999999999999999`;
    const right = `199999999999999999999999999999999999999999`;
    expect(compareIntegerString(left, right)).toBe(-1);
})

test ('compareIntegerString - left is greater than right (both positive)', () => {
    const left  = `199999999999999999999999999999999999999999`;
    const right =  `99999999999999999999999999999999999999999`;
    expect(compareIntegerString(left, right)).toBe(1);
})

test ('compareIntegerString - left is less than right (both positive, same length)', () => {
    const left   = `19999999999999999999999999999999999999998`;
    const right1 = `99999999999999999999999999999999999999999`;
    const right2 = `19999999999999999999999999999999999999999`;
    expect(compareIntegerString(left, right1)).toBe(-1);
    expect(compareIntegerString(left, right2)).toBe(-1);
})

test ('compareIntegerString - left is greater than right (both positive, same length)', () => {
    const left1 = `99999999999999999999999999999999999999998`;
    const left2 = `99999999999999999999999999999999999999999`;
    const right = `19999999999999999999999999999999999999999`;
    expect(compareIntegerString(left1, right)).toBe(1);
    expect(compareIntegerString(left2, right)).toBe(1);
})

test ('compareIntegerString - left is less than right (both negative)', () => {
    const left  = `-999999999999999999999999999999999999999999`;
    const right = `-99999999999999999999999999999999999999999`;
    expect(compareIntegerString(left, right)).toBe(-1);
})

test ('compareIntegerString - left is greater than right (both negative)', () => {
    const left  = `-9999999999999999999999999999999999999999`;
    const right = `-99999999999999999999999999999999999999999`;
    expect(compareIntegerString(left, right)).toBe(1);
})

test ('compareIntegerString - left is less than right (both negative, same length)', () => {
    const left1 = `-99999999999999999999999999999999999999998`;
    const left2 = `-99999999999999999999999999999999999999999`;
    const right = `-19999999999999999999999999999999999999999`;
    expect(compareIntegerString(left1, right)).toBe(-1);
    expect(compareIntegerString(left2, right)).toBe(-1);
})

test ('compareIntegerString - left is greater than right (both negative, same length)', () => {
    const left   = `-19999999999999999999999999999999999999998`;
    const right1 = `-99999999999999999999999999999999999999999`;
    const right2 = `-19999999999999999999999999999999999999999`;
    expect(compareIntegerString(left, right1)).toBe(1);
    expect(compareIntegerString(left, right2)).toBe(1);
})

// test('special test - compare 1 vs 0', () => {
//     const left = "1";
//     const right = "0";
//     expect(compareIntegerString(left, right)).toBe(1);
// })