/// <reference types="jest" />
import getSecondMax from './getSecondMax';
import { testData } from './getSecondMaxTestData'

function runTest(num: number, input: Array<string>, expected: string) {
    test(`test ${num} - [${input}] expects ${expected}`, () => {
        expect(getSecondMax(input)).toBe(expected);
    });
}

testData.map(({input, expected}, idx) => {
    runTest(idx, input, expected);
})

// Manual test/debug
// test('manual', () => {
//     const { input, expected } = testData[6];
//     expect(getSecondMax(input)).toBe(expected);
// })