import largeDigitGenerator from "./largeDigitGenerator";

// Second version, join the 1st and 2nd loop, but update the checking
// basically, if a new max is found, the previous max will be assigned to the second max
// additionally if current element is not the max, but is greater than the second max, then update the second max
// At worst case loop should run at O(n)
function getSecondMax(input: Array<string>) : string {
    const negInfinity = `-${largeDigitGenerator(1024)}`;
    let max : string = negInfinity;
    let output: string = max;
    let current : string = "";

    // if there are less than 2 elements in the array, there will be no 2nd max
    // return -1
    if (input.length < 2) {
        return "-1";
    }

    // if there are only 2 elements in the input array
    if (input.length === 2) {
        const num1 = input[0];
        const num2 = input[1];
        // if either one is not a number means that there is at most only 1 valid number in the array, and no 2nd max
        if (isNaN(Number(num1)) || isNaN(Number(num2))) {
            return "-1";
        }

        // return whichever is lesser as a string type, 
        // or if both are equal, return "-1" (no second max)
        const comparison = compareIntegerString(num1, num2);
        return comparison < 0 ? num1 // num1 < num2, therefore num2 is max and num1 is 2ndMax
             : comparison > 0 ? num2 // num1 > num2, therefore num1 is max and num2 is 2ndMax 
             : "-1"; // num1 === num2, there is no 2ndMax
    }

    // Get first the maximum in the array
    for (let i = 0; i < input.length; i++) {
        current = input[i];
        if (compareIntegerString(current, max) > 0) {
            output = max;
            max = current;
        } else if (
            compareIntegerString(current, output) > 0 &&
            compareIntegerString(current, max) < 0
        ) {
            output = current;
        }
    }

    return output === negInfinity ? "-1" : output;
}


// return -1 if left < right
// return  0 if left === right
// return  1 if left > right
// return NaN if both left and right isNaN
export function compareIntegerString(left: string, right: string) : number {
    const numL = Number(left);
    const numR = Number(right);
    if (isNaN(numL) && !isNaN(numR)) return 1;
    if (!isNaN(numL) && isNaN(numR)) return -1;
    if (isNaN(numL) && isNaN(numR)) return NaN;

    if (numL >= 0 && numR < 0) return 1;
    if (numL < 0 && numR >= 0) return -1;

    if (numL >= 0 && numR >= 0) {
        if (left.length > right.length) return 1;
        if (left.length < right.length) return -1;
        return left.localeCompare(right); // left.length === right.length
    } else { // both are negative 
        if (left.length > right.length) return -1;
        if (left.length < right.length) return 1;
        return left.localeCompare(right) * -1;
    }
}

export default getSecondMax;


// First version created, gets max first in one loop, then gets the second max with another loop
// Each loop should run at Maximum N number of times, at worst it runs 2N times
// O(2n) == O(n)
// function getSecondMax(input: Array<string>) : string {
//     let max : number = -Infinity;
//     let output: number = -Infinity;
//     let current : number = 0;

//     // if there are less than 2 elements in the array, there will be no 2nd max
//     // return -1
//     if (input.length < 2) {
//         return "-1";
//     }

//     // if there are only 2 elements in the input array
//     if (input.length === 2) {
//         const num1 = parseInt(input[0]);
//         const num2 = parseInt(input[1]);
//         // if either one is not a number means that there is at most only 1 valid number in the array, and no 2nd max
//         if (isNaN(num1) || isNaN(num2)) {
//             return "-1";
//         }

//         // return whichever is lesser as a string type, 
//         // or if both are equal, return "-1" (no second max)
//         return num1 > num2 ? `${num2}` : 
//             num2 > num1 ? `${num1}` : "-1";
//     }

//     // Get first the maximum in the array
//     for (let i = 0; i < input.length; i++) {
//         current = parseInt(input[i]);
//         if (current > max) {
//             max = current;
//         }
//     }
    
//     // Go through the array again, this gives us complexity of O(2n) which is same as O(n)
//     for (let i = 0; i < input.length; i++) {
//         current = parseInt(input[i]);
//         // current 
//         if (current > output && current < max) {
//             output = current;
//         }
//     }
//     output = output === -Infinity ? -1 : output;

//     return `${output}`;
// }