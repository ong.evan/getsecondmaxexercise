type getSecondMaxTestDataType = {
    input: Array<string>,
    expected: string
}

export const testData: Array<getSecondMaxTestDataType> = [
    {
        input: ["3", "-2"],
        expected: "-2"
    },
    {
        input: ["5", "5", "4", "2"],
        expected: "4"
    },
    {
        input: ["5", "5", "4", "2", "6"],
        expected: "5"
    },
    {
        input: ["4", "7", "3", "5"],
        expected: "5"
    },
    {
        input: ["4", "4", "4"],
        expected: "-1"
    },
    {
        input: [],
        expected: "-1"
    },
    {
        input: ["0", "-1", "1"],
        expected: "0"
    },
    {
        input: ["-5", "-4", "-3"],
        expected: "-4"
    },
    {
        input: ["3", "4"],
        expected: "3"
    },
    {
        input: ["99", "99"],
        expected: "-1"
    },
    {
        input: ["None", "-3"],
        expected: "-1"
    },
    {
        input: ["8", "Invalid"],
        expected: "-1"
    },
]