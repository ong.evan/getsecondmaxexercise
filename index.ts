// import { getSecondMax_test } from './getSecondMax';

// // Write a function that takes an array of integers given a string as an argument and returns the second max value from the input array. 
// // If there is no second max return -1.Let's take some examples:
// // 1. For array ["3", "-2"] should return "-2"
// // 2. For array ["5", "5", "4", "2"] should return "4"
// // 3. For array ["4", "4", "4"] should return "-1" (duplicates are not considered as the second max)
// // 4. For [] (empty array) should return "-1".
// // Restrictions :CPU complexity should be O(n)
// // Space Complexity should be O(1)
// // You are not allowed to change the array
// // The maximum length of the integer string can be 2^10=1024 digits Feel free to use the language of your choice.This is a very simple problem which we are trying to test if you can write code that is bug-free and take care of all the possible edge cases ensuring that it is production-ready.

// // const largeDigit = largeDigitGenerator(1024);
// // console.log(largeDigit);
// // console.log(parseFloat(largeDigit));

// getSecondMax_test("3", "-2");

console.log('Function tested with JEST, run "npm run test" instead');